class TestoviParser{
 
    constructor(){}
 
    daLiJeJsonString = (string) => {
        try {
            JSON.parse(string);
        }
        catch(exc) {
            return false;
        }
        return true;
    }
 
    dajTacnost(string) {
        if(!new TestoviParser().daLiJeJsonString(string)) {
            return {"tacnost":"0%","greske":"Testovi se ne mogu izvršiti"};
        }
 
        const obj=JSON.parse(string);
        var postotak = (Math.round((obj.stats.passes)/(obj.stats.tests)*1000)/10).toString()+"%";
        const greske =[];
        for(var i=0;i<Object.keys(obj.failures).length;i++) {
            greske.push(obj.failures[i].fullTitle);
        }
        return { "tacnost": postotak, "greske": greske };
         
    }
 
    porediRezultat(string1,string2){
        if(!(new TestoviParser().daLiJeJsonString(string1) && new TestoviParser().daLiJeJsonString(string2))) {
            return {"tacnost":"0%","greske":"Testovi se ne mogu izvršiti"};
        }
        const obj1=JSON.parse(string1);
        const obj2=JSON.parse(string2);
        var x="";
        const greske=[];
        var brtest=0;
        var flag=0;
 
        const porediTestove = (a, b) => { //provjerava da li dva objekta imaju iste atribute neovisno o redoslijedu
        for (const v of new Set([...a, ...b]))
            if (a.filter(e => JSON.stringify(e) == JSON.stringify(v)).length != b.filter(e => JSON.stringify(e) == JSON.stringify(v)).length)
                return false;
        return true;
        };
 
        if(porediTestove(obj1.failures,obj2.failures)) {
            x=(Math.round((obj2.stats.passes)/(obj2.stats.tests)*1000)/10).toString()+"%";
            for(var i=0;i<Object.keys(obj2.failures).length;i++) {
            greske.push(obj2.failures[i].fullTitle);
            }
            return {"promjena": x, "greske": greske};
 
        }
        else {
            for(var i=0;i<Object.keys(obj1.failures).length;i++) {
                flag=0;
                for(var j=0;j<Object.keys(obj2.tests).length;j++) {
                    if(JSON.stringify(obj1.failures[i].fullTitle)==JSON.stringify(obj2.tests[j].fullTitle)) {
                        flag=1;
                        break;
                    }
                }
                if(flag==1) {
                    continue;
                    }
                brtest++;
                greske.push(obj1.failures[i].fullTitle);
                 
            }
            for(var i=0;i<Object.keys(obj2.failures).length;i++)
            greske.push(obj2.failures[i].fullTitle);
             
            x=(Math.round((brtest+obj2.stats.failures)/(brtest+obj2.stats.tests)*1000)/10).toString()+"%";
        }
 
        return {"promjena": x, "greske": greske};
    } 
}
module.exports = TestoviParser;
 
 
 
 