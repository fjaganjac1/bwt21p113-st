const { count } = require('console');
const fs = require('fs');
const http = require('http');
const TestoviParser = require('./TestoviParser');
const Sequelize = require('sequelize');
const sequelize = require('./db');
const GrupaModel = require('./modeli/Grupa');
const VjezbaModel = require('./modeli/Vjezba');
const StudentModel = require('./modeli/Student');

GrupaModel.hasMany(StudentModel);
VjezbaModel.hasMany(StudentModel);
sequelize.sync();


const nePostojiIndex = validateIndex => {  //pomocna funkcija za provjeru postojeceg indeksa u tabeli Student
    return sequelize.StudentModel.findOne({where: {Index: validateIndex}})
    .then(count => {
        if(count !=0) return false;
        return true;
    });
}

const nePostojiGrupa = validateGrupa => {  //pomocna funkcija za provjeru postojeceg indeksa u tabeli Student
    return sequelize.GrupaModel.findOne({where: {Naziv: validateGrupa}})
    .then(count => {
        if(count !=0) return false;
        return true;
    });
}

const server = http.createServer(function(req,res) {
	if(req.url=='/student' && req.method.toUpperCase() =='POST') {
			let buffer = '';
			req.on('data',function(data){
				buffer+=data;
			});
			req.on('end',function(){
				const obj = JSON.parse(buffer);
                if(nePostojiIndex(obj.index)) {
                    if(nePostojiGrupa(obj.grupa)) {
                        GrupaModel.create(JSON.parse(`{"Naziv":${obj.grupa}}`));
                    }
                    StudentModel.create(JSON.parse(`{"Ime":${obj.ime}, "Prezime":${obj.prezime}, "Index":${obj.index}, "Grupa":${obj.grupa}}`));
                    res.writeHead(200, {'Content-Type': 'application/json'});
					res.end(JSON.stringify({status:"Kreiran student!"}));
                }
                else {
                    const postojiIndex = {status:`Student sa indexom ${obj.index} vec postoji`};	
					res.writeHead(200, {'Content-Type': 'application/json'});				 					 
					res.end(JSON.stringify(postojiIndex));
                }
			});	
	}
	//////////////////////////////////////////drugi zad////////////////////////////////////////////
	else if(req.method.toUpperCase()=='PUT') {
		let buffer = '';
		req.on('data',function(data){
			buffer+=data;
		});
		req.on('end',function(){
			let index = req.url.split('/')[2];
			const obj = JSON.parse(buffer);
				if(err) throw err;				

				if(nePostojiIndex(index)) {
					res.writeHead(200, {'Content-Type': 'application/json'});
					res.end(JSON.stringify({status:`Student sa indexom ${index} ne postoji`}));
				}	
                else {
                    if(nePostojiGrupa(obj.grupa)) {
                        GrupaModel.create(JSON.parse(`{"Naziv":${obj.grupa}}`));
                    }
                    StudentModel.update({Grupa:obj.grupa},
                        {where:{Index:index}});
                        
					res.writeHead(200, {'Content-Type': 'application/json'});
					res.end(JSON.stringify({status:`Promjenjena grupa studentu ${index}`}));
				}
							
			
		});
		
	}
	////////////////////////treci zadatak///////////////////////////////////////////////
	else if(req.url=='/batch/student' && req.method.toUpperCase()=='POST') {
		let buffer = '';
		req.on('data',function(data){
			buffer+=data;
		});
		req.on('end', function () {
			let redovi = buffer.split('\n');

            let noviStudenti = [];
            for(let x of redovi) { //pretvara u objekte i stavlja u niz
                noviStudenti.push(JSON.parse(`{"Ime":${x.split(',')[0]},"Prezime":${x.split(',')[1]}, "Index":${x.split(',')[2]}},"Grupa":${x.split(',')[3]}`));
            }

			let batch = '';
            let unbatch = '';
			let N = 0;

            for(let noviStudent of noviStudenti) {  // dodaje u bazu podataka
                if(nePostojiIndex(noviStudent.Index)) {
                    StudentModel.create(noviStudent);
                }
            }
			for(var red in redovi) {   //pravi string za ispis
				if(nePostojiIndex(red.split(',')[2])) {
					N++
				}
				else {
					unbatch+=redovi[red]+'\n';
				}
			}
			   
				
            if(N==redovi.length) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(JSON.stringify({status:`Dodano ${N} studenata`}));
            }
            else if(N==0) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(JSON.stringify({status:`Studenti ${unbatch.slice(0,-1)} su vec upisani`}));
            }
            else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(JSON.stringify({status:`Dodano ${N} studenata, a studenti ${unbatch.slice(0,-1)} su vec upisani`}));

            }				
					
		});
	}
	///////////////////////////cetrvti zad////////////////////////////////////
	else if(req.url=='/vjezbe' && req.method.toUpperCase()=='POST') {
		let buffer = '';
		req.on('data',function(data){
			buffer+=data;
		});
		req.on('end', function() {
			const brojVjezbi = JSON.parse(buffer).brojVjezbi;
            const BrojStudenata = StudentModel.count();
            const IndexiUBazi = StudentModel.findAll({
                attributes: 'Index'
            });
            
            for(var i in IndexiUBazi) {
                for(var j=1;j<brojVjezbi+1;j++) {
                    VjezbaModel.create(JSON.parse(`{"Index":${IndexiUBazi[i]}, "Naziv":${j}}`));
                }
            }
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({status:`Vjezba ${brojVjezbi} uspjesno kreirana!`}));

		});
	}
	/////////////////////////////peti zadatak///////////////////////////////////////
	else if(req.url.includes('/student/')) {
		let buffer = '';
		req.on('data',function(data){
			buffer+=data;
		});
		req.on('end', function(){
			let index = (req.url.split('/')[2]);
			let vjezba = (req.url.split('/')[4]);
			const Parser = new TestoviParser;
			let noviUpis = '';
			let resPromjena;
			let resGreske = [];
			const ProsliTestovi = fs.readFileSync('ProsliTestovi.txt').toString();
			let resTacnost = Parser.dajTacnost(buffer).tacnost;
			const Vjezbe = fs.readFileSync('vjezbe.csv').toString().split('\n');
			if(!Vjezbe.toString().includes(`${index},`)) {
				res.writeHead(200, {'Content-Type': 'application/json'});				 					 
				res.end(JSON.stringify({status: `Student ${index} ne postoji`}));

			}
			else if(!Vjezbe.toString().includes(`${vjezba},`)) {
				res.writeHead(200, {'Content-Type': 'application/json'});				 					 
				res.end(JSON.stringify({status: `Vjezba ${vjezba} ne postoji`}));
			}
			else {
			if(ProsliTestovi==''){
				for(var red in Vjezbe) {
					if(Vjezbe[red].split(',')[0]==index && Vjezbe[red].split(',')[1]==vjezba) {
						noviUpis+=`${index},${vjezba},${resTacnost},[${Parser.dajTacnost(buffer).greske.toString()}]\n`;
					}
					else {
						noviUpis+=Vjezbe[red]+'\n';
					}
				}
			}
			else {
				for(var red in Vjezbe) {
				let PoredjeniRez = Parser.porediRezultat(buffer,ProsliTestovi);
				if(Vjezbe[red].split(',')[0]==index && Vjezbe[red].split(',')[1]==vjezba) {
					resPromjena=PoredjeniRez.promjena;
					resGreske = PoredjeniRez.greske;
					noviUpis+=`${index},${vjezba},${resPromjena},[${resGreske}]\n`;
				}
				else {
					noviUpis+=Vjezbe[red]+'\n';
				}
				}				
			}
			fs.truncate("ProsliTestovi.txt", 0, function() {   
				fs.writeFile("ProsliTestovi.txt", buffer, function (err) {
					if (err) throw err;
				});
			});
			fs.truncate("vjezbe.csv", 0, function() {       
				fs.writeFile("vjezbe.csv", noviUpis, function (err) {
					if (err) throw err;
				});
			});
  
			const Response = {vjezba: vjezba, tacnost: resTacnost, promjena: resPromjena, greske: resGreske};	
			res.writeHead(200, {'Content-Type': 'application/json'});				 					 
			res.end(JSON.stringify(Response));
		}
		});

	}
}).listen(1001);

module.exports = server;