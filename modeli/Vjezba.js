const Sequelize = require('sequelize');
const sequelize = require('../db');

    const Vjezba = sequelize.define("Vjezba", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull:false,
            primaryKey: true
        },

        Index: {
            type: Sequelize.STRING,
            allowNull:false,
            references: 'Student',
            referenceKey: 'Index',
            validate: {
                notEmpty: true
            }
        },

        Naziv: {
            type: Sequelize.INTEGER,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },

        Tacnost: {
            type: Sequelize.STRING,
            allowNull: false,
            default: '0%',
            validate: {
                notEmpty: true
            }
        },

        Promjena: {
            type: Sequelize.STRING,
            allowNull: false,
            default: '0%',
            validate: {
                notEmpty: true
            }
        },

        Greske: {
            type: Sequelize.STRING,
            allowNull: true,
            default: null,
            validate: {
                notEmpty: true
            }
        },

        Testovi: {
            type: Sequelize.STRING,
            allowNull: true,
            default: null,
            validate: {
                notEmpty: true
            }
        }
    });


