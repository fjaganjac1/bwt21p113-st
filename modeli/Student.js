const Sequelize = require('sequelize');
const sequelize = require('../db');

    const Student = sequelize.define("Student", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull:false,
            primaryKey: true
        },
        Ime: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        Prezime: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        Index: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        Grupa: {
            type: Sequelize.STRING,
            references: 'Grupa',
            referenceKey: 'Naziv',
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    });

    module.exports = Student;

