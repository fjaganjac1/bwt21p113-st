const Sequelize = require('sequelize');
const sequelize = require('../db');

    const Grupa = sequelize.define("Grupa", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull:false,
            primaryKey: true
        },
        Naziv: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    });


